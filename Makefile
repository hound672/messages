RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
$(eval $(RUN_ARGS):;@:)

LOCAL_BIN:=$(CURDIR)/bin
GOLANGCI_TAG:=1.54.2
PATH:=$(PATH):$(LOCAL_BIN)
GOPRIVATE:="gitlab.ptsecurity.com"
GOPROXY:="http-repo.ptsecurity.ru/artifactory/goproxy"

.PHONY: .bin-deps
.bin-deps:
	$(info Installing binary dependencies...)
	mkdir -p $(LOCAL_BIN)
	GOBIN=$(LOCAL_BIN) go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.31.0

# install golangci-lint binary
.PHONY: install-lint
install-lint:
ifeq ($(wildcard $(GOLANGCI_BIN)),)
	$(info Downloading golangci-lint v$(GOLANGCI_TAG))
	GOBIN=$(LOCAL_BIN) go install github.com/golangci/golangci-lint/cmd/golangci-lint@v$(GOLANGCI_TAG)
GOLANGCI_BIN:=$(LOCAL_BIN)/golangci-lint
endif

.PHONY: install
install: .bin-deps install-lint

.PHONY: linter_proto
linter_proto:
	buf lint

.PHONY: quality
quality: linter_proto

.PHONY: clean_cache
clean_cache:
	go clean -cache

.PHONY: generate
generate: .gen-proto

.PHONY: .gen-proto
.gen-proto: linter_proto
	protoc -I ./api \
	--plugin=protoc-gen-go=$(LOCAL_BIN)/protoc-gen-go --go_out=. --go_opt=paths=source_relative \
	messages/dast/start_scan_request.proto \
	messages/dast/start_scan_response.proto
