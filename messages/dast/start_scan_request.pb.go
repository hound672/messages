// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.31.0
// 	protoc        v4.25.1
// source: messages/dast/start_scan_request.proto

package dast

import (
	_ "buf.build/gen/go/bufbuild/protovalidate/protocolbuffers/go/buf/validate"
	_ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2/options"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type StartScanRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	WorkflowUuid string `protobuf:"bytes,1,opt,name=workflow_uuid,json=workflowUuid,proto3" json:"workflow_uuid,omitempty"`
	SiteUrl      string `protobuf:"bytes,2,opt,name=site_url,json=siteUrl,proto3" json:"site_url,omitempty"`
}

func (x *StartScanRequest) Reset() {
	*x = StartScanRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_messages_dast_start_scan_request_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StartScanRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StartScanRequest) ProtoMessage() {}

func (x *StartScanRequest) ProtoReflect() protoreflect.Message {
	mi := &file_messages_dast_start_scan_request_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StartScanRequest.ProtoReflect.Descriptor instead.
func (*StartScanRequest) Descriptor() ([]byte, []int) {
	return file_messages_dast_start_scan_request_proto_rawDescGZIP(), []int{0}
}

func (x *StartScanRequest) GetWorkflowUuid() string {
	if x != nil {
		return x.WorkflowUuid
	}
	return ""
}

func (x *StartScanRequest) GetSiteUrl() string {
	if x != nil {
		return x.SiteUrl
	}
	return ""
}

var File_messages_dast_start_scan_request_proto protoreflect.FileDescriptor

var file_messages_dast_start_scan_request_proto_rawDesc = []byte{
	0x0a, 0x26, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2f, 0x64, 0x61, 0x73, 0x74, 0x2f,
	0x73, 0x74, 0x61, 0x72, 0x74, 0x5f, 0x73, 0x63, 0x61, 0x6e, 0x5f, 0x72, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0d, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67,
	0x65, 0x73, 0x2e, 0x64, 0x61, 0x73, 0x74, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f,
	0x61, 0x70, 0x69, 0x2f, 0x66, 0x69, 0x65, 0x6c, 0x64, 0x5f, 0x62, 0x65, 0x68, 0x61, 0x76, 0x69,
	0x6f, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x63,
	0x2d, 0x67, 0x65, 0x6e, 0x2d, 0x6f, 0x70, 0x65, 0x6e, 0x61, 0x70, 0x69, 0x76, 0x32, 0x2f, 0x6f,
	0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1b, 0x62, 0x75, 0x66, 0x2f, 0x76, 0x61,
	0x6c, 0x69, 0x64, 0x61, 0x74, 0x65, 0x2f, 0x76, 0x61, 0x6c, 0x69, 0x64, 0x61, 0x74, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xdc, 0x01, 0x0a, 0x10, 0x53, 0x74, 0x61, 0x72, 0x74, 0x53,
	0x63, 0x61, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x72, 0x0a, 0x0d, 0x77, 0x6f,
	0x72, 0x6b, 0x66, 0x6c, 0x6f, 0x77, 0x5f, 0x75, 0x75, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x42, 0x4d, 0x92, 0x41, 0x3f, 0x32, 0x15, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x74, 0x20,
	0x77, 0x6f, 0x72, 0x6b, 0x66, 0x6c, 0x6f, 0x77, 0x20, 0x55, 0x55, 0x49, 0x44, 0x4a, 0x26, 0x22,
	0x32, 0x34, 0x33, 0x38, 0x61, 0x63, 0x33, 0x63, 0x2d, 0x33, 0x37, 0x65, 0x62, 0x2d, 0x34, 0x39,
	0x30, 0x32, 0x2d, 0x61, 0x64, 0x65, 0x66, 0x2d, 0x65, 0x64, 0x31, 0x36, 0x62, 0x34, 0x34, 0x33,
	0x31, 0x30, 0x33, 0x30, 0x22, 0xe0, 0x41, 0x02, 0xba, 0x48, 0x05, 0x72, 0x03, 0xb0, 0x01, 0x01,
	0x52, 0x0c, 0x77, 0x6f, 0x72, 0x6b, 0x66, 0x6c, 0x6f, 0x77, 0x55, 0x75, 0x69, 0x64, 0x12, 0x54,
	0x0a, 0x08, 0x73, 0x69, 0x74, 0x65, 0x5f, 0x75, 0x72, 0x6c, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x42, 0x39, 0x92, 0x41, 0x2d, 0x32, 0x15, 0x54, 0x61, 0x72, 0x67, 0x65, 0x74, 0x27, 0x73, 0x20,
	0x55, 0x52, 0x4c, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x73, 0x63, 0x61, 0x6e, 0x4a, 0x14, 0x22, 0x68,
	0x74, 0x74, 0x70, 0x73, 0x3a, 0x2f, 0x2f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x63, 0x6f,
	0x6d, 0x22, 0xe0, 0x41, 0x02, 0xba, 0x48, 0x03, 0xc8, 0x01, 0x01, 0x52, 0x07, 0x73, 0x69, 0x74,
	0x65, 0x55, 0x72, 0x6c, 0x42, 0x3c, 0x5a, 0x3a, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x70,
	0x74, 0x73, 0x65, 0x63, 0x75, 0x72, 0x69, 0x74, 0x79, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x61, 0x70,
	0x70, 0x73, 0x65, 0x63, 0x2d, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x6d, 0x6f,
	0x64, 0x65, 0x6c, 0x73, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2f, 0x64, 0x61,
	0x73, 0x74, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_messages_dast_start_scan_request_proto_rawDescOnce sync.Once
	file_messages_dast_start_scan_request_proto_rawDescData = file_messages_dast_start_scan_request_proto_rawDesc
)

func file_messages_dast_start_scan_request_proto_rawDescGZIP() []byte {
	file_messages_dast_start_scan_request_proto_rawDescOnce.Do(func() {
		file_messages_dast_start_scan_request_proto_rawDescData = protoimpl.X.CompressGZIP(file_messages_dast_start_scan_request_proto_rawDescData)
	})
	return file_messages_dast_start_scan_request_proto_rawDescData
}

var file_messages_dast_start_scan_request_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_messages_dast_start_scan_request_proto_goTypes = []interface{}{
	(*StartScanRequest)(nil), // 0: messages.dast.StartScanRequest
}
var file_messages_dast_start_scan_request_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_messages_dast_start_scan_request_proto_init() }
func file_messages_dast_start_scan_request_proto_init() {
	if File_messages_dast_start_scan_request_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_messages_dast_start_scan_request_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StartScanRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_messages_dast_start_scan_request_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_messages_dast_start_scan_request_proto_goTypes,
		DependencyIndexes: file_messages_dast_start_scan_request_proto_depIdxs,
		MessageInfos:      file_messages_dast_start_scan_request_proto_msgTypes,
	}.Build()
	File_messages_dast_start_scan_request_proto = out.File
	file_messages_dast_start_scan_request_proto_rawDesc = nil
	file_messages_dast_start_scan_request_proto_goTypes = nil
	file_messages_dast_start_scan_request_proto_depIdxs = nil
}
